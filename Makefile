BIOS_DIR=/bios

all:boot.bin

boot.bin:boot.asm bios/disk.asm $(BIOS_DIR)/print.inc
	nasm boot.asm -fbin -o boot.bin	

run:boot.bin
	qemu-system-x86_64 boot.bin

clean:
	rm boot.bin