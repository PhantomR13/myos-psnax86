; print function
print:
	cld	
	
	print_char_loop:
		lodsb
		cmp AL, 0
		je print_done
		
		mov AH, 0x0E
		mov BH, 0
		int 0x10
		jmp print_char_loop

	print_done:
		ret
;;;;;;;;;;;;;;;;
