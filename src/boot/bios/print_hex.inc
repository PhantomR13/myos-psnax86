; Prints the number in DX in hexadecimal format.
print_hex:
	push BP

	mov CX, 0
	mov AX, 0

	loop_digits:
		mov BX, DX	; load a copy of the number
	
		shl BX, CL	; bring digit on the most significant nibble
		shr BX, 12	; bring it to the least significant

		mov BL, [HEX_TABLE + BX]
		mov BP, AX
		mov [HEX_PATTERN + BP + 2], BL ; store current digit in the pattern

		add CL, 4
		inc AX
		cmp AX, 4
		jl loop_digits

	pop BP

	mov SI, HEX_PATTERN
	call print ; print the modified pattern

	ret

HEX_TABLE db "0123456789ABCDEF"
HEX_PATTERN db "0x****", 13, 10, 0
	