; boot.asm
[ORG 0x7c00]

section .text
	global main

main:
; disable interrupts ; TODO: re-enable them in the kernel?
cli

; setup segment registers
xor AX, AX
mov SS, AX,
mov DS, AX,
mov ES, AX
mov FS, AX
mov GS, AX

; set left->right direction
cld

mov SI, FIRSTSTAGE_STARTING_MSG
call print

; read two more sectors, jump to second stage bootloader
mov AX, 0
mov ES, AX
mov BX, 0x7c00 + 512 ; ES:BX - dbuf addr
mov AL, 3			 ; num sectors
mov DL, 0x80 		 ; drive
mov CH, 0 			 ; starting cyl
mov DH, 0			 ; starting head
mov CL, 2			 ; starting sector
call readSectorsFromDisk

; jump to second stage bootloader
mov SI, JUMPING_TO_SECONDSTAGE_MSG
call print
jmp second_stage

hang:
	jmp hang

%include "bios/print.inc"
%include "bios/disk.inc"

FIRSTSTAGE_STARTING_MSG db 'First stage bootloader started.', 13, 10, 0
JUMPING_TO_SECONDSTAGE_MSG db 'Jumping to second stage bootloader...', 13, 10, 0
READING_SECOND_SECTOR db 'Reading second sector from disk...', 13, 10, 10

times 510 - ($ - $$) db 0
db 0x55
db 0xAA 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;; SECOND SECTOR ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

second_stage:
mov SI, SECOND_STAGE_READ_SUCCESSFUL_MSG
call print


call try_enabling_A20

call check_if_cpuid_supported

call check_if_long_mode_usable

call switch_to_long_mode

mov SI, LEAVING_MSG
call print

jmp hang

%include "enableA20.inc"
%include "testA20.inc"		
%include "check_if_cpuid_supported.inc"
%include "check_if_long_mode_usable.inc"
%include "long_mode.inc"
%include "gdt64.inc"

%include "bios/print_hex.inc"
%include "bios/debug.inc"

LEAVING_MSG db 'Leaving bootloader...', 13, 10, 0
SECOND_STAGE_READ_SUCCESSFUL_MSG db 'Second stage bootloader started.', 13, 10, 0


; Use 64-bit.
[BITS 64]
 
Realm64:
    cli                           ; Clear the interrupt flag.
    mov ax, GDT64.Data            ; Set the A-register to the data descriptor.
    mov ds, ax                    ; Set the data segment to the A-register.
    mov es, ax                    ; Set the extra segment to the A-register.
    mov fs, ax                    ; Set the F-segment to the A-register.
    mov gs, ax                    ; Set the G-segment to the A-register.
    mov ss, ax                    ; Set the stack segment to the A-register.
    mov edi, 0xB8000              ; Set the destination index to 0xB8000.
    mov rax, 0x1F201F201F201F20   ; Set the A-register to 0x1F201F201F201F20.
    mov ecx, 500                  ; Set the C-register to 500.
    rep stosq                     ; Clear the screen.
	
		%define	VIDMEM	0xB8000		; video memory
 
	mov	edi, VIDMEM		; get pointer to video memory
	mov	byte [edi], 'A'		; print character 'A'
	mov	byte [edi+1], 0x7		; character attribute
    hlt                           ; Halt the processor.

; times 1020 - ($ - second_stage) db 0
