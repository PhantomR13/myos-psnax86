check_if_cpuid_supported:
	pushfd
	pop EAX

	mov ECX, EAX ; ECX contains initial flags register
	
	; try changing ID flag
	xor EAX, 1 << 21
	push EAX
	popfd


	pushfd
	pop EAX
	cmp EAX, ECX ; compare initial flags with flags after attempted flip
	je cpuid_not_supported

	mov SI, CPUID_SUPPORTED_MSG
	call print
	ret

	cpuid_not_supported:
		mov SI, CPUID_NOT_SUPPORTED_MSG
		call print
		jmp hang
		ret


CPUID_SUPPORTED_MSG db "CPUID instruction supported.", 13, 10, 0

CPUID_NOT_SUPPORTED_MSG db "[FATAL] CPUID not supported.", 13, 10, 0