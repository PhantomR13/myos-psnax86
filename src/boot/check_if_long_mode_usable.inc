check_if_long_mode_usable:
	mov EAX, 0x80000000
	cpuid
	cmp EAX, 0x80000001
	jb long_mode_not_usable

	mov EAX, 0x80000001
	cpuid
	test EDX, 1 << 29
	jz long_mode_not_usable

	mov SI, LONG_MODE_USABLE
	call print
	ret


	long_mode_not_usable:
		mov SI, LONG_MODE_NOT_USABLE
		call print
		jmp hang
		ret

LONG_MODE_USABLE db "Long mode supported.", 13, 10, 0
LONG_MODE_NOT_USABLE db "[FATAL] Long mode not supported.", 13, 10, 0