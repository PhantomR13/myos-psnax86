; Tests whether the A20 line is enabled. 
; Returns: AL <- 1 if enabled, 0 if disabled.
; Compares the bootsector signature at 0x07c0:0x01fe=0x07dfe with 
; what's at address 107dfe = 0xffff:0x7e0e and then 
; rotates the signature and checks again.
test_A20_enabled:
	push ES
	; simple comparison
	mov AX, 0x07C0
	mov ES, AX
	mov SI, 0x01FE
	mov BX, [ES:SI]
	; mov DX, BX
	; call print_hex

	mov AX, 0xFFFF
	mov ES, AX
	mov DI, 0x7E0E

	mov CX, [ES:DI]
	; mov DX, CX
	; call print_hex

	cmp BX, CX
	jne enabled

	; rotate and compare again
	mov AX, 0x07C0
	mov ES, AX
	mov SI, 0x01FE
	mov AX, [ES:SI]
	rol AX, 8
	mov [ES:SI], AX
	
	mov BX, [ES:SI]
	; mov DX, BX
	; call print_hex

	mov AX, 0xFFFF
	mov ES, AX
	mov DI, 0x7E0E

	mov CX, [ES:DI]
	; mov DX, CX
	; call print_hex

	cmp BX, CX
	jne enabled

	; both checks passed, assume A20 is disabled
	mov AL, 0
	pop ES
	ret

	enabled:
		mov AL, 1
		pop ES
		ret
